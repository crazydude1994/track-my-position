import webapp2
from model import Coordinate
import google.appengine.ext.webapp.template as template
from google.appengine.ext.ndb import GeoPt
from google.appengine.api.datastore_errors import BadValueError

class MainHandler(webapp2.RequestHandler):
    def get(self):
        coordinates = Coordinate.query().fetch()
        self.response.write(template.render('map.html', {"points" : coordinates}))
        
        
class LogPosition(webapp2.RequestHandler):
    def get(self):
        temp = Coordinate()
        try:
            temp.geo = GeoPt(self.request.get('lat'), self.request.get('long'))
            temp.alt = float(self.request.get('alt')) or None
            temp.speed = float(self.request.get('speed')) or None
            temp.time_utc = self.request.get('time') or None
            temp.put()
        except BadValueError as E:
            self.error(200)
            self.response.write(E.message)
        else:
            self.response.write('Logged')
            
            
def cache(f):
    def wrapper(self):
        pass
    return wrapper

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/log', LogPosition),
], debug=True)
