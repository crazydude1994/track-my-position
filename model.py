from google.appengine.ext import ndb

class Coordinate(ndb.Model):
    geo = ndb.GeoPtProperty(required = True)
    alt = ndb.FloatProperty()
    speed = ndb.FloatProperty()
    time_utc = ndb.StringProperty()
    time = ndb.DateTimeProperty(auto_now = True)
    user = ndb.UserProperty(auto_current_user = True)